-- This is how we process the file
function ProcessFile(FileName)
   local Out = FILread(FileName)
   iguana.log("Read "..FileName.." ("..#Out.." bytes)");
   queue.push{data=Out}
   LastFile = FileName
   if component.live() then os.remove(FileName) end                           
end