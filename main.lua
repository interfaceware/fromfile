require "Status"
require "ProcessFile"
require "MatchRules"
require "FIL.FILreadWrite"

function PollDir(Dir)
   local LastFile
   for FileName in os.fs.glob(Dir.."*") do
      if MatchFile(FileName) then
         LastFile = FileName
         ProcessFile(FileName)
      end
   end
   return LastFile -- For status
end

function main(Data)
   local Polltime = 10;
   -- Schedule the next polling event
   component.setTimer{delay=Polltime*1000}
   if (Data == "INIT") then
      iguana.log("Polling every "..Polltime.." seconds");
      return
   end
   local Dir = component.fields().SourceDir
   trace(Polltime)
   if not os.fs.access(Dir) then
      component.setStatus{
         data=STATerrorBlock("Directory "..Dir.." does not exist.")
      }
   else
      local LastFile = PollDir(Dir)
      component.setStatus{data=Status(Polltime, LastFile)} 
   end  
end