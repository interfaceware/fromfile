require "STAT.STATstatus"

function Status(Polltime, FileName)
   local R =''
   if (FileName) then
      R = R..STATrow("Last file processed", FileName)
   end
   R = R .. STATrow("Running next at", os.date("%H:%M:%S", os.time()+Polltime));
   return R
end