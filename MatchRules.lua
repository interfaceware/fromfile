--What file extensions do we match?
local FileExtensionMatchSet={
   txt=true,
   log=true,
   hl7=true
}

function MatchFile(FileName)
   --local JustFileName = FILfilename(FileName)
   local Extn = FILextension(FileName)
   Extn = Extn:lower()
   return FileExtensionMatchSet[Extn]
end

--[[
Don't feel limited by these choices - you can apply what ever matching rules
make sense for your purpose.  Rembember you have access to regex matching, lookups
and more.
]]